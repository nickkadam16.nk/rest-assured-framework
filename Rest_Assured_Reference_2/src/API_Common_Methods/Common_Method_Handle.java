package API_Common_Methods;

import static io.restassured.RestAssured.given;

public class Common_Method_Handle {
	
	public static int get_statusCode(String endpoint) {

		int statusCode = given().when().get(endpoint).then().extract().response().statusCode();
		return statusCode;

	}

	public static String get_responseBody(String endpoint) {

		String responseBody = given().when().get(endpoint).then().extract().response().asString();
		return responseBody;

	}

	public static int post_statusCode(String endpoint, String requestBody) {

		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().response().statusCode();
		return statusCode;

	}

	public static String post_responseBody(String endpoint, String requestBody) {

		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().response().asString();
		return responseBody;

	}

	public static int put_statusCode(String endpoint, String requestBody) {

		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().statusCode();
		return statusCode;

	}

	public static String put_responseBody(String endpoint, String requestBody) {

		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().asString();
		return responseBody;

	}

}
