package API_Utility_Common_Methods;

import java.io.File;

public class Handle_Dir {
	
	public static File create_log_dir(String log_dir) {
		
//		Fetch the current project directory
		String project_dir = System.getProperty("user.dir");
//		System.out.println("the current project path is : "+project_dir);
		
		File directory = new File(project_dir+"\\API_Logs\\"+log_dir);
		
		if(directory.exists()) {
			directory.delete();
//			System.out.println(directory+" is deleted");
			directory.mkdir();
//			System.out.println(directory+" is created");
		}else {
			directory.mkdir();
//			System.out.println(directory+" is created");
		}
		return directory;
	}
	
}
