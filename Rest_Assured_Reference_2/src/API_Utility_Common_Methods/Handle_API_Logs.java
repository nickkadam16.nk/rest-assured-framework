package API_Utility_Common_Methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_Logs {

	public static void evidence_creator(File logDirName, String fileName, String requestBody, String endpoint,
			String responseBody) throws IOException {

		File newFile = new File(logDirName + "\\" + fileName + ".txt");
//		System.out.println("To save request & response body we've created new file name :" + newFile.getName());

		FileWriter dataWriter = new FileWriter(newFile);
		dataWriter.write("Request Body is : " + requestBody + "\n\n");
		dataWriter.write("Endpoint is : " + endpoint + "\n\n");
		dataWriter.write("Response Body is : " + responseBody);
		dataWriter.close();
	}
}
