package Rest_API;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.given;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class Get2_API {
	
	public static void main(String[] args) {
		
//		declare the base URL
		RestAssured.baseURI = "https://reqres.in/";
		
//		trigger the API and capture the response body
		String responseBody = given()
		.when().get("api/users?page=2")
		.then().extract().response().asString();
//		System.out.println(responseBody);
		
//		expected data
		int exp_id[] = {7,8,9,10,11,12};
		String exp_email[] = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in",
				"george.edwards@reqres.in","rachel.howell@reqres.in"};
		String exp_fName[] = {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String exp_lName[] = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		
//		parse the response body
		JSONObject JO = new JSONObject(responseBody);
//		System.out.println(JO);
		JSONArray JArr = JO.getJSONArray("data");
//		System.out.println(JArr);
		int count = JArr.length();
//		System.out.println(count);
		
		for(int i=0;i<count;i++) {
			
			int res_id = JArr.getJSONObject(i).getInt("id");
			System.out.println(res_id);
			String res_email = JArr.getJSONObject(i).getString("email");
			String res_fName = JArr.getJSONObject(i).getString("first_name");
			String res_lName = JArr.getJSONObject(i).getString("last_name");
			
//			validate the response body
			Assert.assertEquals(exp_id[i], res_id);
			Assert.assertEquals(exp_email[i], res_email);
			Assert.assertEquals(exp_fName[i], res_fName);
			Assert.assertEquals(exp_lName[i], res_lName);
				
		}
		
	}

}
