This is my Rest Assured Framework

my framework is a combination of keyword driven framework and wherein i'm having test scripts, test cases which are also being executed on the basis of data.
my framework can be divided into six major components 
1) Driving Mechanism
2) Test Scripts
3) Common Functions
4) Data file/ Variable file / Excel File
5) Libraries
6) Reports

- in Driving Mechanism i've developed two types of mechanism static driver class and dynamic driver class.

- in Static driver class as the name suggest we are statictically calling the test scripts into a main method and executing them which is not advisable for that reason i've developed new method called dynamic driver class.

- in Dynamic driver class we fetched test cases to be executed from user from an excel file and dynamically call the corresponding test scripts on runtime, this is done by using java.lang.reflect.
then we have commpn functions in this we have two categories
i) API related common functions
ii) Utilities

- in API related common functions we have the common functions which are use to trigger the API extract the response body and extract the status code.

- then we have Utilities section under the utilities we create utilities to create the directory when it is does not exists for test log files.

- and if at all log directory exists we deleted and created new log directory.

- and then we have another utility which will take automatically take the endpoint, requestbody responsebody and respected headers and added into a notepad file for each single test script.

- then we have one more utility which will read the values from an excel file.

- then we are using excel file to input the data to our framework or the requestbody parameters are entered into a excel file.

- then the libraries which are using are rest assured, testNG, org.json, apachePOI.

- And for generating reports we are using allure-testNG, extentreports libraries.

- the dependency management library are been handled by maven repository.
